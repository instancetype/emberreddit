/**
 * Created by instancetype on 9/29/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
import Ember from 'ember';
import ajax from 'ic-ajax';

export default Ember.Object.extend({
  find : function(name, id) {
    return ajax('http://www.reddit.com/r/' + id + '.json')
      .then(function(result) {
        return result.data.children.map(function(c) {
          return {
            id : c.id,
            title : c.data.title,
            domain : c.data.domain,
            url : c.data.url
          }
        });
      });
  }
});