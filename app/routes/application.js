/**
 * Created by instancetype on 9/29/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
import Ember from 'ember';

export default Ember.Route.extend({
  model : function() {
    return ['javascript', 'nodejs', 'emberjs']
  }
})